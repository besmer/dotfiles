import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO

myTerminal      = "alacritty"
myManageHook    = composeAll
    [   className =? "Gimp"      --> doFloat
    ]

main = do
    xmproc <- spawnPipe "xmobar /home/gaurang/.config/xmonad/xmobarrc"
    xmonad $ defaultConfig
        {   manageHook = manageDocks <+> myManageHook <+> manageHook defaultConfig,
            layoutHook = avoidStruts  $  layoutHook defaultConfig,
            handleEventHook    = handleEventHook defaultConfig <+> docksEventHook,
            logHook = dynamicLogWithPP xmobarPP
                        {   ppOutput = hPutStrLn xmproc,
                            ppTitle = xmobarColor "green" "" . shorten 50
                        },
            modMask = mod4Mask,          -- Rebind Mod to the Windows key
            terminal = myTerminal
        }       `additionalKeys`
        [   ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock"),
            ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s"),
            ((0, xK_Print), spawn "scrot")
        ]
